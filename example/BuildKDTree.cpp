/**
 * @file    BuildKDTree.cpp
 * @brief   Build a k-d tree using sample data
 * @author  Yao Chen
 */

#include "../src/KDTree.h"
#include "../src/Configuration.h"

#include <iostream>
#include <unordered_map>

using namespace std;
using namespace kdtree;

int main(const int argc, const char* argv[]) {
    /// parse the argument for configuration file
    string configPath = "../../config.cfg";
    if (argc == 2) {
        configPath = boost::lexical_cast<string>(argv[1]);
    }

    /// define a shared pointer to the configuration
    boost::shared_ptr<Configuration> sharedcConfig = boost::make_shared<Configuration>(configPath);

    /// set up k-d tree spilitting option
    std::unordered_map<string, Options> optionsDict;
    optionsDict["CYCLE"] = Options::CYCLE;
    optionsDict["MAX_RANGE"] = Options::MAX_RANGE;
    if (optionsDict.find(sharedcConfig->splittingOption()) == optionsDict.end()) {
        cerr << "Wrong option! Please choose CYCLE or MAX_RANGE!" << endl;
        exit(-1);
    }

    /// load sample data
    const string sampleDataPath = sharedcConfig->inputDir() + "/" + sharedcConfig->sampleFilename();
    PointsWithIndices<double> points = parseCSV<double>(sampleDataPath);

    /// build the k-d tree
    SharedNode<double> root = createKDTree(points, optionsDict[sharedcConfig->splittingOption()]);

    /// serialize and save the k-d tree
    const string kDTreePath = sharedcConfig->outputDir() + "/" + sharedcConfig->kDTreeFilename();
    KDTreeNode<double>::saveTree(root, kDTreePath);

    return 0;
}
