/**
 * @file    Query1NN.cpp
 * @brief   Find the nearest neighbor in the sample data given a target point using a k-d tree.
 * @author  Yao Chen
 */

#include "../src/KDTree.h"
#include "../src/Configuration.h"

#include <fstream>
#include <iostream>

using namespace std;
using namespace kdtree;

int main(const int argc, const char* argv[]) {
    /// parse the argument for configuration file
    string configPath = "../../config.cfg";
    if (argc == 2) {
        configPath = boost::lexical_cast<string>(argv[1]);
    }

    /// define a shared pointer to the configuration
    boost::shared_ptr<Configuration> sharedcConfig = boost::make_shared<Configuration>(configPath);

    /// load k-d tree
    const string kDTreePath = sharedcConfig->outputDir() + "/" + sharedcConfig->kDTreeFilename();
    SharedNode<double> root = KDTreeNode<double>::loadTree(kDTreePath);

    /// load query data
    const string queryDataPath = sharedcConfig->inputDir() + "/" + sharedcConfig->queryFilename();
    PointsWithIndices<double> points = parseCSV<double>(queryDataPath);

    /// find the nearest neighbor for the target point
    const string queryResultFilename = sharedcConfig->outputDir() + "/" + sharedcConfig->queryResultFilename();
    ofstream ofs(queryResultFilename);
    for (const auto& point : points) {
        SharedNode<double> node = query1NN<double>(point.first, root);
        double dist = sqrt(distance<double>((*node->pointWithIndex()).first, point.first));
        size_t index = (*node->pointWithIndex()).second;
        ofs << index << " " << dist << endl;
    }

    return 0;
}
