/**
 * @file    QueryKNN.cpp
 * @brief   Find k nearest neighbors in the sample data given a target using a k-d tree.
 * @author  Yao Chen
 */

#include "../src/KDTree.h"
#include "../src/Configuration.h"

#include <fstream>
#include <iostream>

using namespace std;
using namespace kdtree;

int main(const int argc, const char* argv[]) {
    /// parse the argument for configuration file
    string configPath = "../../config.cfg";
    if (argc == 2) {
        configPath = boost::lexical_cast<string>(argv[1]);
    }

    /// define a shared pointer to the configuration
    boost::shared_ptr<Configuration> sharedcConfig = boost::make_shared<Configuration>(configPath);

    /// load k-d tree
    const string kDTreePath = sharedcConfig->outputDir() + "/" + sharedcConfig->kDTreeFilename();
    SharedNode<double> root = KDTreeNode<double>::loadTree(kDTreePath);

    /// load query data
    const string queryDataPath = sharedcConfig->inputDir() + "/" + sharedcConfig->queryFilename();
    PointsWithIndices<double> points = parseCSV<double>(queryDataPath);

    /// find the k nearest neighbors for the first point in the query samples
    auto point = points[0]; // target point
    size_t k = sharedcConfig->k();
    auto nodes = queryKNN<double>(point.first, root, k);
    auto pq =  nodes.pq();
    for (auto iter = pq.begin() ; iter != pq.end(); ++iter) {
        double dist = sqrt(distance<double>((*iter->second->pointWithIndex()).first, point.first));
        size_t index = (*iter->second->pointWithIndex()).second;
        cout << index << " " << dist << endl;
    }

    return 0;
}
