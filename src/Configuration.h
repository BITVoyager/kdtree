/**
 * @file    Configuration.h
 * @brief   Configuration file for kdtree project
 * @author  Yao Chen
 */

#pragma once

#include <string>

namespace kdtree {

class Configuration {
public:
    /**
     * @brief   Default Constructor
     */
    Configuration() : splittingOption_("CYCLE"), inputDir_("../../data/"), sampleFilename_("input.csv"), queryFilename_("output.csv"),
           outputDir_("../../output/"), kDTreeFilename_("kdtree.dat"), queryResultFilename_("query_result.txt"), k_(1) {}

    /**
     * @brief   Parse parameters from configuration file (.cfg)
     * @param   filename: filename of the configuration file
     */
    Configuration(const std::string& filename);

public:
    /* getters */
    std::string splittingOption() const { return splittingOption_; }
    std::string inputDir() const { return inputDir_; }
    std::string sampleFilename() const { return sampleFilename_; }
    std::string queryFilename() const { return queryFilename_; }
    std::string outputDir() const { return outputDir_; }
    std::string kDTreeFilename() const { return kDTreeFilename_; }
    std::string queryResultFilename() const { return queryResultFilename_; }
    size_t k() const { return k_; }

private:
    std::string splittingOption_;       // splitting option: CYCLE or MAX_RANGE
    std::string inputDir_;              // input directory
    std::string sampleFilename_;        // sample data
    std::string queryFilename_;         // query data
    std::string outputDir_;             // output directory
    std::string kDTreeFilename_;        // filename of the stored k-d tree
    std::string queryResultFilename_;   // filename of the query output
    size_t k_;                          // k nearest neighbors
};

} // namespace kdtree
