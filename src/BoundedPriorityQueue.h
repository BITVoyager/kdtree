/**
 * @file    BoundedPriorityQueue.h
 * @brief   Define the data structure of bounded priority queue, which is in fact a priority queue with capacity (maximum size).
 * @author  Yao Chen
 */

#pragma once

#include <map>
#include <algorithm>
#include <limits>
#include <iostream>

namespace kdtree {

template<class KeyType, class ValueType>
class BoundedPriorityQueue {
public:
    /* default constructor */
    BoundedPriorityQueue() : capacity_(0) {}

    /**
     * @brief   Constructor with paramters
     * @param   capacity     The maximum size of the bounded priority queue
     */
    BoundedPriorityQueue(const size_t capacity) : capacity_(capacity) {}

    /* copy constructor and copy assignment operator */
    BoundedPriorityQueue(const BoundedPriorityQueue& rhs) = default;
    BoundedPriorityQueue& operator=(const BoundedPriorityQueue& rhs) = default;

    /* move constructor and move assignment operator */
    BoundedPriorityQueue(BoundedPriorityQueue&& rhs) = default;
    BoundedPriorityQueue& operator=(BoundedPriorityQueue&& rhs) = default;

    /* destructor */
    virtual ~BoundedPriorityQueue() = default;

public:
    /**
     * @brief   Push a key-value pair into the queue
     * @param   key     Key of the pair
     * @param   value   Value of the paird
     */
    void push(const KeyType& key, const ValueType& value) {
        /// push the key-value pair into the queue
        pq_.emplace(key, value);

        /// check whether the queue has already reached the capacity in O(1)
        if (pq_.size() > capacity_) {
            auto last = pq_.end();
            --last;
            pq_.erase(last);
        }
    }

    /**
     * @brief   Return the value with smallest key and pop it from the queue
     * @return  Value with smallest key
     */
    ValueType pop() {
        if (pq_.empty()) {
            std::cerr << "The queue is empty!" << std::endl;
            exit(-1);
        }

        ValueType res = pq_.begin()->second;
        pq_.erase(pq_.begin());
        return res;
    }

    /**
     * @brief   Get the size of the bounded priority queue
     * @return  Size of the bounded priority queue
     */
    size_t size() const { return pq_.size(); }

    /**
     * @brief   Get the capacity of the bounded priority queue
     * @return  Capacity of the bounded priority queue
     */
    size_t capacity() const { return capacity_; }

    /**
     * @brief   Check whether the bounded priority queue is empty
     * @return  True if the bounded priority queue is empty; Otherwise, false
     */
    bool empty() const { return pq_.empty(); }

    /**
     * @brief   Get the smallest key in the bounded priority queue
     * @return  Smallest key in the bounded priority queue
     */
    KeyType minKey() const { return this->empty() ? std::numeric_limits<KeyType>::max() : pq_.begin()->first; }

    /**
     * @brief   Get the greatest key in the bounded priority queue
     * @return  Greatest key in the bounded priority queue
     */
    KeyType maxKey() const { return this->empty() ? std::numeric_limits<KeyType>::max() : pq_.rbegin()->first; }

    /**
     * @brief   Get the priority queue
     * @return  Priority queue
     */
    std::multimap<KeyType, ValueType> pq() const { return pq_; }


private:
    std::multimap<KeyType, ValueType> pq_;  ///< priority queue
    size_t capacity_;                       ///< capacity of the bounded priority queue
};

} // namespace kdtree

