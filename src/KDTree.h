﻿/**
 * @file    KDTree.h
 * @brief   Define data structures for the k-d tree
 * @author  Yao Chen
 */

#pragma once

#include "Utils.h"
#include "BoundedPriorityQueue.h"

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/optional.hpp>
#include <boost/serialization/split_member.hpp>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <iostream>

namespace kdtree {

/* k-d tree node */
template<class ElemType>
class KDTreeNode {
public:
    /* short hand definition of a shared pointer to a k-d tree node */
    using SharedKDTreeNode = boost::shared_ptr<KDTreeNode<ElemType>>;

    /**
     * @brief   Default constructor
     */
    KDTreeNode() : splittingAxis_(0), splittingValue_(static_cast<ElemType>(0)) {
        left_ = SharedKDTreeNode(nullptr);
        right_ = SharedKDTreeNode(nullptr);
        pointWithIndex_ = boost::none;
    }

    /**
     * @brief   Constructor with user defined inputs
     * @param   axis        Splitting axis
     * @param   splitValue  Splitting value
     * @param   index       Index of the point
     * @param   left        Shared pointer to the root of left subtree
     * @param   right       Shared pointer to the root of right subtree
     * @param   point       Optional point if the node is the leaf
     */
    KDTreeNode(const size_t splittingAxis, const ElemType& splittingValue, const SharedKDTreeNode& left,
               const SharedKDTreeNode& right, const OptionalPointWithIndex<ElemType>& pwi) :
        splittingAxis_(splittingAxis), splittingValue_(splittingValue), left_(left), right_(right), pointWithIndex_(pwi) {}

    /* copy constructor and copy assignment operator */
    KDTreeNode(const KDTreeNode& rhs) = default;
    KDTreeNode& operator=(const KDTreeNode& rhs) = default;

    /* move constructor and move assignment operator */
    KDTreeNode(KDTreeNode&& rhs) = default;
    KDTreeNode& operator=(KDTreeNode&& rhs) = default;

    /**
     * @brief   Default destructor
     * @note    In case some user wants to derive from this class, the destructor is declared as virtual.
     */
    virtual ~KDTreeNode() = default;

public:
    /* getters */
    const size_t splittingAxis() const { return splittingAxis_; }
    const ElemType splittingValue() const { return splittingValue_; }
    const SharedKDTreeNode left() const { return left_; }
    const SharedKDTreeNode right() const  { return right_; }
    const OptionalPointWithIndex<ElemType> pointWithIndex() const { return pointWithIndex_; }

    /* setters */
    void splittingAxis(const size_t sa) { splittingAxis_ = sa; }
    void splittingValue(const ElemType& sv) { splittingValue_ = sv; }
    void left(const SharedKDTreeNode& l) { left_ = l; }
    void right(const SharedKDTreeNode& r) { right_ = r; }
    void pointWithIndex(const OptionalPointWithIndex<ElemType>& pwi) { pointWithIndex_ = pwi; }

public:
    /* I/O methods */
    static void saveTree(const SharedKDTreeNode& root, const std::string& filename) {
        // write out binary archive
        std::ofstream ofs(filename.c_str());
        if(!ofs) {
            std::cerr << "Could not open archive file for writing: " << filename << std::endl;
            exit(1);
        }
        boost::archive::binary_oarchive oa(ofs);
        oa << root;
    }

    static SharedKDTreeNode loadTree(const std::string& filename) {
        // write out binary archive
        std::ifstream ifs(filename.c_str());
        if(!ifs) {
            std::cerr << "Could not open archive file for reading: " << filename << std::endl;
            exit(1);
        }
        boost::archive::binary_iarchive ia(ifs);
        SharedKDTreeNode root;
        ia >> root;

        return root;
    }


private:
    /* serialization function */
    friend class boost::serialization::access;

    template<class ARCHIVE>
    void save(ARCHIVE& ar, const unsigned int version) const {
        ar & boost::serialization::make_nvp("sa", splittingAxis_);
        ar & boost::serialization::make_nvp("sv", splittingValue_);

        const KDTreeNode* left = left_.get();
        ar & boost::serialization::make_nvp("left", left);

        const KDTreeNode* right = right_.get();
        ar & boost::serialization::make_nvp("right", right);

        const bool flag = pointWithIndex_.is_initialized();
        ar & boost::serialization::make_nvp("pwi_flag", flag);
        if (flag) {
            ar & boost::serialization::make_nvp("pwi_pt", (*pointWithIndex_).first);
            ar & boost::serialization::make_nvp("pwi_idx", (*pointWithIndex_).second);
        }
    }

    template<class ARCHIVE>
    void load(ARCHIVE& ar, const unsigned int version) {
        ar & boost::serialization::make_nvp("sa", splittingAxis_);
        ar & boost::serialization::make_nvp("sv", splittingValue_);

        KDTreeNode* left;
        ar & boost::serialization::make_nvp("left", left);
        left_ = SharedKDTreeNode(left);

        KDTreeNode* right;
        ar & boost::serialization::make_nvp("right", right);
        right_ = SharedKDTreeNode(right);

        bool flag = false;
        ar & boost::serialization::make_nvp("pwi_flag", flag);
        if (flag) {
            decltype((*pointWithIndex_).first) point;
            size_t index;
            ar & boost::serialization::make_nvp("pwi_pt", point);
            ar & boost::serialization::make_nvp("pwi_idx", index);
            pointWithIndex_ = std::make_pair(point, index);
        } else {
            pointWithIndex_ = boost::none;
        }
    }

    BOOST_SERIALIZATION_SPLIT_MEMBER()

private:
    size_t splittingAxis_;      // splitting axis
    ElemType splittingValue_;   // splitting value
    SharedKDTreeNode left_;     // shared pointer to left child
    SharedKDTreeNode right_;    // shared pointer to right child
    OptionalPointWithIndex<ElemType> pointWithIndex_;   // optional point and index (if it is a leaf node)
};

/* Options for selecting good splitting axis */
enum class Options {
    CYCLE,      // cycle through each axis
    MAX_RANGE   // axis with maximum range
};

/* short-hand definition of a shared pointer to the k-d tree node */
template<class ElemType>
using SharedNode = typename KDTreeNode<ElemType>::SharedKDTreeNode;

/**
 * @brief   Helper function of creating the k-d tree using the parsed data points
 * @param   pointsByIndex   Points parsed from the csv file.
 * @param   depth           Depth of k-d tree
 * @param   option          Option for selecting good splitting axis
 * @return  The root of the constructed k-d tree
 */
template<class ElemType>
SharedNode<ElemType> createKDTreeHelper(PointsWithIndices<ElemType>& pointsByIndex, const int depth, const Options& option) {
    const size_t n = pointsByIndex.size();
    if (n == 0) return SharedNode<ElemType>(nullptr);
    const size_t k = pointsByIndex[0].first.size();

    /// construct the k-d tree node if there is only one element
    if (n == 1) {
        return boost::make_shared<KDTreeNode<ElemType>>(0, static_cast<ElemType>(0), SharedNode<ElemType>(nullptr), SharedNode<ElemType>(nullptr),
                                                        OptionalPointWithIndex<ElemType>(pointsByIndex[0]));
    }

    /// find splitting axis
    size_t splittingAxis = 0;
    if (option == Options::CYCLE) {
        splittingAxis = depth % k;
    } else {
        ElemType maxRange = static_cast<ElemType>(-1.0);
        for (size_t i = 0; i < k; ++i) {
            auto pointsByAxis = transposePoints(pointsByIndex);
            auto tempRange = findRange(pointsByAxis[i]);
            if (tempRange > maxRange) {
                maxRange = tempRange;
                splittingAxis = i;
            }
        }
    }

    /// find splitting value and the data points for both categories
    sortByAxis(pointsByIndex, splittingAxis);
    ElemType splittingValue = static_cast<ElemType>(0);
    PointsWithIndices<ElemType> leftPoints;
    PointsWithIndices<ElemType> rightPoints;
    if (n % 2 == 0) {
        splittingValue = (pointsByIndex[n / 2 - 1].first[splittingAxis] + pointsByIndex[n / 2].first[splittingAxis]) / static_cast<ElemType>(2);
        leftPoints = PointsWithIndices<ElemType>(pointsByIndex.begin(), pointsByIndex.begin() + n / 2);
        rightPoints = PointsWithIndices<ElemType>(pointsByIndex.begin() + n / 2, pointsByIndex.end());

    } else {
        splittingValue = pointsByIndex[n / 2].first[splittingAxis];
        leftPoints = PointsWithIndices<ElemType>(pointsByIndex.begin(), pointsByIndex.begin() + n / 2 + 1);
        rightPoints = PointsWithIndices<ElemType>(pointsByIndex.begin() + n / 2 + 1, pointsByIndex.end());
    }

    /// construct the tree recursively
    SharedNode<ElemType> root = boost::make_shared<KDTreeNode<ElemType>>(splittingAxis, splittingValue, SharedNode<ElemType>(nullptr), SharedNode<ElemType>(nullptr), boost::none);
    (*root).left(createKDTreeHelper(leftPoints, depth + 1, option));
    (*root).right(createKDTreeHelper(rightPoints, depth + 1, option));
    return root;
}

/**
 * @brief   Create the k-d tree using the parsed data points
 * @param   pointsByIndex   Points parsed from the csv file
 * @param   option          Option for selecting good splitting axis
 * @return  The root of the constructed k-d tree
 */
template<class ElemType>
SharedNode<ElemType> createKDTree(PointsWithIndices<ElemType>& pointsByIndex, const Options& option) {
    return createKDTreeHelper<ElemType>(pointsByIndex, 0, option);
}

/**
 * @brief   Helper function of finding the nearest neighbor (1NN) of the query point in the given k-d tree
 * @param   target      Query point
 * @param   root        Root of a k-d tree
 * @param   nearestNode Bookkeeping of the nearest node
 * @param   bestDist    Bookkeeping of the best distance
 */
template<class ElemType>
void query1NNHelper(const Point<ElemType>& target, const SharedNode<ElemType>& root, SharedNode<ElemType>& nearestNode, ElemType& bestDist) {
    if (root.get() == nullptr) return;
    if (root->pointWithIndex() != boost::none) {
        PointWithIndex<ElemType> guess = (*(root->pointWithIndex()));
        const ElemType dist = distance(target, guess.first);
        if (dist < bestDist) {
            bestDist = dist;
            nearestNode = root;
        }
        return;
    }

    const size_t spliittingAxis = root->splittingAxis();
    const ElemType splittingValue = root->splittingValue();
    if (target[spliittingAxis] < splittingValue) query1NNHelper(target, root->left(), nearestNode, bestDist);
    else query1NNHelper(target, root->right(), nearestNode, bestDist);

    const ElemType temp = target[spliittingAxis] - splittingValue;
    if (temp * temp < bestDist) {
        if (target[spliittingAxis] < splittingValue) query1NNHelper(target, root->right(), nearestNode, bestDist);
        else query1NNHelper(target, root->left(), nearestNode, bestDist);
    }
}

/**
 * @brief   Find the nearest neighbor (1NN) of the query point in the given k-d tree
 * @param   target  Query point
 * @param   root    Root of a k-d tree
 * @return  The nearest neigbour in the given k-d tree
 */
template<class ElemType>
SharedNode<ElemType> query1NN(const Point<ElemType>& target, const SharedNode<ElemType>& root) {
    SharedNode<ElemType> result;
    ElemType bestDist = std::numeric_limits<ElemType>::max();
    query1NNHelper(target, root, result, bestDist);
    return result;
}

/**
 * @brief   Helper function of finding k nearest neighbors (kNNs) of the query point in the given k-d tree
 * @param   target      Query point
 * @param   root        Root of a k-d tree
 * @param   bpq         Bounded priority queue
 */
template<class ElemType>
void queryKNNHelper(const Point<ElemType>& target, const SharedNode<ElemType>& root, BoundedPriorityQueue<ElemType, SharedNode<ElemType>>& bpq) {
    if (root.get() == nullptr) return;

    if (root->pointWithIndex() != boost::none) {
        PointWithIndex<ElemType> guess = (*(root->pointWithIndex()));
        const ElemType key = distance(target, guess.first);
        bpq.push(key, root);
        return;
    }

    const size_t spliittingAxis = root->splittingAxis();
    const ElemType splittingValue = root->splittingValue();
    if (target[spliittingAxis] < splittingValue) queryKNNHelper(target, root->left(), bpq);
    else queryKNNHelper(target, root->right(), bpq);

    const ElemType temp = target[spliittingAxis] - splittingValue;
    if (bpq.size() < bpq.capacity() || temp * temp < bpq.maxKey()) {
        if (target[spliittingAxis] < splittingValue) queryKNNHelper(target, root->right(), bpq);
        else queryKNNHelper(target, root->left(), bpq);
    }
}

/**
 * @brief   Find k nearest neighbors (kNNs) of the query point in the given k-d tree
 * @param   target  Query point
 * @param   root    Root of a k-d tree
 * @param   k       K nearest neighbors
 * @return  The nearest neigbour in the given k-d tree. Note that the result is in the bounded priority queue.
 *          For more detailed usage of the restun value, please refer to QueryKNN.cpp in example/.
 */
template<class ElemType>
BoundedPriorityQueue<ElemType, SharedNode<ElemType>> queryKNN(const Point<ElemType>& target, const SharedNode<ElemType>& root, const size_t k) {
    BoundedPriorityQueue<ElemType, SharedNode<ElemType>> bpq(k);
    queryKNNHelper(target, root, bpq);
    return bpq;
}

} // namespace kdtree
