/**
 * @file    Configuration.cpp
 * @brief   Configuration file for kdtree project
 * @author  Yao Chen
 */

#include "Configuration.h"

#include <boost/program_options.hpp>

#include <fstream>

using namespace std;
namespace po = boost::program_options;

namespace kdtree {

/* *************************************************************** */
Configuration::Configuration(const std::string &filename) {
    po::options_description config("Configuration");
    config.add_options()
        ("splitting_option", po::value<string>(&splittingOption_)->default_value("CYCLE"), "Option for selecting the splitting axis")
        ("input_dir", po::value<string>(&inputDir_)->default_value("../../data/"), "Input data directory")
        ("sample_file_name",  po::value<string>(&sampleFilename_)->default_value("sample_data.csv"), "Path to sample data")
        ("query_file_name",  po::value<string>(&queryFilename_)->default_value("query_data.csv"), "Path to query data")
        ("output_dir",  po::value<string>(&outputDir_)->default_value("../../output/"), "Output directory")
        ("kd_tree_file_name",  po::value<string>(&kDTreeFilename_)->default_value("kdtree.dat"), "k-d tree")
        ("query_result_file_name",  po::value<string>(&queryResultFilename_)->default_value("query_result.txt"), "Query result")
        ("k",  po::value<size_t>(&k_)->default_value(1), "k nearest neighbors");

    ifstream ifs(filename.c_str());
    po::variables_map vm;
    po::store(po::parse_config_file(ifs, config), vm);
    po::notify(vm);
}

} // namespace kdtree
