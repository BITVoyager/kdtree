/**
 * @file    Utils.h
 * @brief   Utility functions for the k-d tree project
 * @author  Yao Chen
 */

#pragma once

#include <boost/optional/optional.hpp>
#include <boost/lexical_cast.hpp>

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>

namespace kdtree {

/* short-hand definition of a k-demensional point */
template<class ElemType>
using Point = std::vector<ElemType>;

/* short-hand definition of n k-demensional points */
template<class ElemType>
using Points = std::vector<Point<ElemType>>;

/* short-hand definition of a k-demensional point with index */
template<class ElemType>
using PointWithIndex = std::pair<Point<ElemType>, size_t>;

/* short-hand definition of an optional k-demensional point with index */
template<class ElemType>
using OptionalPointWithIndex = boost::optional<PointWithIndex<ElemType>>;

/* short-hand definition of k-demensional points with indices */
template<class ElemType>
using PointsWithIndices = std::vector<PointWithIndex<ElemType>>;

/**
 * @brief   Parse the csv file
 * @param   filename The filename of the csv file
 * @return  N points of k-dimensions, where N is the total number of points and k is the dimension of the point
 */
template<class ElemType>
PointsWithIndices<ElemType> parseCSV(const std::string& filename) {
    std::ifstream ifs(filename);
    std::string line;
    PointsWithIndices<ElemType> points;
    size_t i = 0;
    while (std::getline(ifs, line)) {
        std::istringstream iss(line);
        std::string cell;
        Point<ElemType> point;
        while (std::getline(iss, cell, ',')) {
            point.push_back(boost::lexical_cast<ElemType>(cell));
        }
        points.push_back(make_pair(point, i));
        ++i;
    }
    return points;
}

/**
 * @brief   Transpose the points either from N-by-k to k-by-N or from k-by-N to N-by-k
 * @param   points to be transposed
 * @return  Transposed points
 */
template<class ElemType>
Points<ElemType> transposePoints(const PointsWithIndices<ElemType>& points) {
    const std::size_t m = points.size();
    if (m == 0) return Points<ElemType>();
    const std::size_t n = points[0].first.size();
    if (n == 0) return Points<ElemType>();

    Points<ElemType> result(n, Point<ElemType>(m));
    for (std::size_t i = 0; i < n; ++i) {
        for (std::size_t j = 0; j < m; ++j) {
            result[i][j] = points[j].first[i];
        }
    }
    return result;
}

/**
 * @brief   Find median in a 1-D vector
 * @param   point   Input vector
 * @return  The median in the input vector
 */
template<class ElemType>
ElemType findMedian(Point<ElemType> point) {
    const size_t n = point.size();
    if (n == 0) return static_cast<ElemType>(0);
    std::nth_element(point.begin(), point.begin() + n / 2, point.end());

    if (n % 2 == 1) return point[n / 2];
    else {
        ElemType value = point[n / 2];
        std::nth_element(point.begin(), point.begin() + n / 2 - 1, point.end());
        return (value + point[n / 2 - 1]) / static_cast<ElemType>(2);
    }
}

/**
 * @brief   Sort the points by axis
 * @param   points  Points to be sorted
 * @param   axis    Sorting axis
 */
template<class ElemType>
struct ComparatorByAxis {
    size_t axis_;
    ComparatorByAxis() : axis_(0) {}
    ComparatorByAxis(const size_t axis) : axis_(axis) {}
    bool operator()(const PointWithIndex<ElemType>& a, const PointWithIndex<ElemType>& b) { return a.first[axis_] < b.first[axis_]; }
};

template<class ElemType>
void sortByAxis(PointsWithIndices<ElemType>& pwi, const size_t axis) {
    const size_t n = pwi.size();
    if (n == 0) return;
    const size_t k = pwi[0].first.size();
    if (k == 0) return;
    std::sort(pwi.begin(), pwi.end(), ComparatorByAxis<ElemType>(axis));
}

/**
 * @brief   Find the range (max value - min value) in a vector
 * @param   point   The vector
 * @return  range of the vector
 */
template<class ElemType>
ElemType findRange(const Point<ElemType>& point) {
    auto maxValue = *std::max_element(point.begin(), point.end());
    auto minValue = *std::min_element(point.begin(), point.end());
    return maxValue - minValue;
}

/**
 * @brief   Calculate the distance (2-norm) between two points
 * @note    The input points have the same dimension.
 * @param   a   Point a
 * @param   b   Point b
 * @return  Eucleadian distance between a and b
 */
template<class ElemType>
ElemType distance(const Point<ElemType>& a, const Point<ElemType>& b) {
    ElemType dist = static_cast<ElemType>(0);
    for (int i = 0; i < a.size(); ++i) {
        dist += (a[i] - b[i]) * (a[i] - b[i]);
    }
    return dist;
}

/**
 * @brief   Brute force search for nearest neighbor. It is used as the benchmark for testing.
 * @param   points   Sample points
 * @param   target   point to be searched
 * @return  Index of nearest neighbor and best distance between target and the nearest neighbor
 */
template<class ElemType>
std::pair<size_t, ElemType> bruteForce1NN(const PointsWithIndices<ElemType>& points, const Point<ElemType>& target) {
    size_t bestIndex = 0;
    ElemType bestDist = static_cast<ElemType>(1e9);
    for (int i = 0; i < points.size(); ++i) {
        auto dist = distance(points[i].first, target);
        if (dist < bestDist) {
            bestDist = dist;
            bestIndex = points[i].second;
        }
    }
    return std::make_pair(bestIndex, bestDist);
}

/**
 * @brief   Brute force search for k nearest neighbors. It is used as the benchmark for testing.
 * @param   points   Sample points
 * @param   target   point to be searched
 * @return  Index of nearest neighbor and best distance between target and the nearest neighbor
 */
template<class ElemType>
std::multimap<ElemType, size_t> bruteForceKNN(const PointsWithIndices<ElemType>& points, const Point<ElemType>& target) {
    std::multimap<ElemType, size_t> pq;
    for (int i = 0; i < points.size(); ++i) {
        auto dist = distance(points[i].first, target);
        size_t index = points[i].second;
        pq.emplace(dist, index);
    }

    return pq;
}

} // namespace kdtree
