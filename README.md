# K-D Tree #
This repository implements the data structure of the k-dimensional tree (k-d tree) based on **C++11**. The data structure is templated on the scalar type of the input data, so it can handle different precisions. Moreover, the interface is easy to use. 

### Implementation ###
In addition to some new features in C++11, I utilized some functionality in the library **boost**: shared pointer, serialization, program options and lexical cast. I did not use `std::shared_ptr` because `boost::shared_ptr` is better supported in serialization in boost version 1.54. I believe my implementation should support `std::shared_ptr` because the serialization is totally based on raw pointers.

The library is organized as follows

* `data/`:sample and query data in `.csv` format
* `doc/`: documentation, see `kdtree.pdf`
* `example/`: example code for executables
* `output/`: left empty to store the output files (serialized k-d tree and k-d tree query result)
* `src/`: source code
* `test/`: all unit tests
* `config.cfg`: configuration file.

### What is this library for? ###

* Construct a k-d tree from sample data in a static way [O(nlog^2(n))]
* Query a target in the k-d tree [O(log(n))]
* Save/load the k-d tree to/from disk

### Prerequisites ###
* Boost 1.54 `sudo apt-get install libboost-all-dev`


### How do I get set up? ###

```
#!Linux
cd /path/to/kdtree
mkdir build
cd build
cmake ..
make check (optional, run unit tests)
make -j4
```

### How do I run the executables? ###
Suppose you are at `/path/to/kdtree/build` and you have already specified the hyper parameters in `/path/to/kdtree/config.cfg`. You can try:
```
#!Linux
cd example
./build_kdtree
./query_kdtree
./query_brute_force_search (optional, run benchmark)
```