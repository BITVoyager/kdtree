#include "../src/CppUnitLite/TestHarness.h"
#include "../src/KDTree.h"

using namespace std;
using namespace kdtree;

TEST(CretaeKDTree, createKDTreeCycle) {
    PointsWithIndices<double> points = { {{ 1.0, 2.0, 3.0 }, 0},
                              { { 4.0, 5.0, 6.0 }, 1 },
                              { { 7.0, 8.0, 9.0 }, 2 },
                              { { 0.0, -5.0, 10.0 }, 3}};
    random_shuffle(points.begin(), points.end());

    SharedNode<double> root = createKDTree<double>(points, Options::CYCLE);
    EXPECT_LONGS_EQUAL(0, root->splittingAxis());
    EXPECT_DOUBLES_EQUAL(2.5, root->splittingValue(), 1e-9);
    EXPECT(root->left().get() != nullptr);
    EXPECT(root->right().get() != nullptr);
    EXPECT(root->pointWithIndex() == boost::none);

    SharedNode<double> left = root->left();
    EXPECT_LONGS_EQUAL(1, left->splittingAxis());
    EXPECT_DOUBLES_EQUAL(-1.5, left->splittingValue(), 1e-9);
    EXPECT(left->left().get() != nullptr);
    EXPECT(left->right().get() != nullptr);
    EXPECT(left->pointWithIndex() == boost::none);

    SharedNode<double> right = root->right();
    EXPECT_LONGS_EQUAL(1, right->splittingAxis());
    EXPECT_DOUBLES_EQUAL(6.5, right->splittingValue(), 1e-9);
    EXPECT(right->left().get() != nullptr);
    EXPECT(right->right().get() != nullptr);
    EXPECT(right->pointWithIndex() == boost::none);

    SharedNode<double> ll = left->left();
    EXPECT_LONGS_EQUAL(0, ll->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, ll->splittingValue(), 1e-9);
    EXPECT(ll->left().get() == nullptr);
    EXPECT(ll->right().get() == nullptr);
    EXPECT(ll->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*ll->pointWithIndex()).first[0], 0.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*ll->pointWithIndex()).first[1], -5.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*ll->pointWithIndex()).first[2], 10.0, 1e-9);
    EXPECT_LONGS_EQUAL((*ll->pointWithIndex()).second, 3);

    SharedNode<double> lr = left->right();
    EXPECT_LONGS_EQUAL(0, lr->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, lr->splittingValue(), 1e-9);
    EXPECT(lr->left().get() == nullptr);
    EXPECT(lr->right().get() == nullptr);
    EXPECT(lr->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*lr->pointWithIndex()).first[0], 1.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*lr->pointWithIndex()).first[1], 2.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*lr->pointWithIndex()).first[2], 3.0, 1e-9);
    EXPECT_LONGS_EQUAL((*lr->pointWithIndex()).second, 0);

    SharedNode<double> rl = right->left();
    EXPECT_LONGS_EQUAL(0, rl->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, rl->splittingValue(), 1e-9);
    EXPECT(rl->left().get() == nullptr);
    EXPECT(rl->right().get() == nullptr);
    EXPECT(rl->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*rl->pointWithIndex()).first[0], 4.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rl->pointWithIndex()).first[1], 5.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rl->pointWithIndex()).first[2], 6.0, 1e-9);
    EXPECT_LONGS_EQUAL((*rl->pointWithIndex()).second, 1);

    SharedNode<double> rr = right->right();
    EXPECT_LONGS_EQUAL(0, rr->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, rr->splittingValue(), 1e-9);
    EXPECT(rr->left().get() == nullptr);
    EXPECT(rr->right().get() == nullptr);
    EXPECT(rr->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*rr->pointWithIndex()).first[0], 7.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rr->pointWithIndex()).first[1], 8.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rr->pointWithIndex()).first[2], 9.0, 1e-9);
    EXPECT_LONGS_EQUAL((*rr->pointWithIndex()).second, 2);

}

TEST(CretaeKDTree, createKDTreeMaxRange) {
    PointsWithIndices<float> points = { {{ 1.0, 2.0, 3.0 }, 0},
                              { { 4.0, 5.0, 6.0 }, 1 },
                              { { 7.0, 8.0, 9.0 }, 2 },
                              { { 0.0, -5.0, 10.0 }, 3}};
    random_shuffle(points.begin(), points.end());

    SharedNode<float> root = createKDTree<float>(points, Options::MAX_RANGE);
    EXPECT_LONGS_EQUAL(1, root->splittingAxis());
    EXPECT_DOUBLES_EQUAL(3.5, root->splittingValue(), 1e-9);
    EXPECT(root->left().get() != nullptr);
    EXPECT(root->right().get() != nullptr);
    EXPECT(root->pointWithIndex() == boost::none);

    SharedNode<float> left = root->left();
    EXPECT_LONGS_EQUAL(1, left->splittingAxis());
    EXPECT_DOUBLES_EQUAL(-1.5, left->splittingValue(), 1e-9);
    EXPECT(left->left().get() != nullptr);
    EXPECT(left->right().get() != nullptr);
    EXPECT(left->pointWithIndex() == boost::none);

    SharedNode<float> right = root->right();
    EXPECT_LONGS_EQUAL(0, right->splittingAxis());
    EXPECT_DOUBLES_EQUAL(5.5, right->splittingValue(), 1e-9);
    EXPECT(right->left().get() != nullptr);
    EXPECT(right->right().get() != nullptr);
    EXPECT(right->pointWithIndex() == boost::none);

    SharedNode<float> ll = left->left();
    EXPECT_LONGS_EQUAL(0, ll->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, ll->splittingValue(), 1e-9);
    EXPECT(ll->left().get() == nullptr);
    EXPECT(ll->right().get() == nullptr);
    EXPECT(ll->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*ll->pointWithIndex()).first[0], 0.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*ll->pointWithIndex()).first[1], -5.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*ll->pointWithIndex()).first[2], 10.0, 1e-9);
    EXPECT_LONGS_EQUAL((*ll->pointWithIndex()).second, 3);

    SharedNode<float> lr = left->right();
    EXPECT_LONGS_EQUAL(0, lr->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, lr->splittingValue(), 1e-9);
    EXPECT(lr->left().get() == nullptr);
    EXPECT(lr->right().get() == nullptr);
    EXPECT(lr->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*lr->pointWithIndex()).first[0], 1.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*lr->pointWithIndex()).first[1], 2.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*lr->pointWithIndex()).first[2], 3.0, 1e-9);
    EXPECT_LONGS_EQUAL((*lr->pointWithIndex()).second, 0);

    SharedNode<float> rl = right->left();
    EXPECT_LONGS_EQUAL(0, rl->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, rl->splittingValue(), 1e-9);
    EXPECT(rl->left().get() == nullptr);
    EXPECT(rl->right().get() == nullptr);
    EXPECT(rl->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*rl->pointWithIndex()).first[0], 4.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rl->pointWithIndex()).first[1], 5.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rl->pointWithIndex()).first[2], 6.0, 1e-9);
    EXPECT_LONGS_EQUAL((*rl->pointWithIndex()).second, 1);

    SharedNode<float> rr = right->right();
    EXPECT_LONGS_EQUAL(0, rr->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, rr->splittingValue(), 1e-9);
    EXPECT(rr->left().get() == nullptr);
    EXPECT(rr->right().get() == nullptr);
    EXPECT(rr->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*rr->pointWithIndex()).first[0], 7.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rr->pointWithIndex()).first[1], 8.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rr->pointWithIndex()).first[2], 9.0, 1e-9);
    EXPECT_LONGS_EQUAL((*rr->pointWithIndex()).second, 2);
}

TEST(Query, query1NN) {
    PointsWithIndices<float> points = { { { 1.0, 2.0, 3.0 }, 0},
                                        { { 4.0, 5.0, 6.0 }, 1 },
                                        { { 7.0, 8.0, 9.0 }, 2 },
                                        { { 0.0, -5.0, 10.0 }, 3},
                                        { { 19.0, 0.8, -20.0 }, 4} };


    random_shuffle(points.begin(), points.end());
    PointsWithIndices<float> backups(points.begin(), points.end());
    SharedNode<float> root = createKDTree<float>(points, Options::MAX_RANGE);

    for (int i = 0; i < backups.size(); ++i) {
        SharedNode<float> res = query1NN<float>(backups[i].first, root);
        float bestDist = distance<float>((*res->pointWithIndex()).first, backups[i].first);
        EXPECT_DOUBLES_EQUAL(0.0, bestDist, 1e-9);
        EXPECT_LONGS_EQUAL(backups[i].second, (*res->pointWithIndex()).second);
    }

    /// compare the 1NN query with brute force search
    Point<float> t0 = { 0.0, 0.0, 0.0 };
    SharedNode<float> kDTreeRes = query1NN<float>(t0, root);
    float kDTreedist = distance<float>((*kDTreeRes->pointWithIndex()).first, t0);
    size_t kDTreeIndex = (*kDTreeRes->pointWithIndex()).second;

    auto bruteForceRes = bruteForce1NN(points, t0);
    EXPECT_DOUBLES_EQUAL(bruteForceRes.second, kDTreedist, 1e-9);
    EXPECT_LONGS_EQUAL(bruteForceRes.first, kDTreeIndex);

}

TEST(Query, queryKNNLargeK) {
    PointsWithIndices<float> points = { { { 1.0, 2.0, 3.0 }, 0},
                                        { { 4.0, 5.0, 6.0 }, 1 },
                                        { { 7.0, 8.0, 9.0 }, 2 },
                                        { { 0.0, -5.0, 10.0 }, 3},
                                        { { 19.0, 0.8, -20.0 }, 4} };


    random_shuffle(points.begin(), points.end());
    SharedNode<float> root = createKDTree<float>(points, Options::MAX_RANGE);

    /// compare the 1NN query with brute force search
    Point<float> t0 = { 0.0, 0.0, 0.0 };
    auto kDTreeRes = queryKNN<float>(t0, root, 6);
    auto pqKDTreeRes = kDTreeRes.pq();
    auto pqBruteForceRes = bruteForceKNN(points, t0);

    auto iter1 = pqKDTreeRes.begin();
    auto iter2 = pqBruteForceRes.begin();
    EXPECT_LONGS_EQUAL(5, kDTreeRes.size());
    for (int i = 0; i < 5; ++i) {

        float dist = distance<float>((*iter1->second->pointWithIndex()).first, t0);
        size_t index = (*iter1->second->pointWithIndex()).second;

        EXPECT_DOUBLES_EQUAL(iter2->first, dist, 1e-9);
        EXPECT_LONGS_EQUAL(iter2->second, index);
        ++iter1;
        ++iter2;
    }
}

TEST(Query, queryKNN) {
    PointsWithIndices<float> points = { { { 1.0, 2.0, 3.0 }, 0},
                                        { { 4.0, 5.0, 6.0 }, 1 },
                                        { { 7.0, 8.0, 9.0 }, 2 },
                                        { { 0.0, -5.0, 10.0 }, 3},
                                        { { 19.0, 0.8, -20.0 }, 4} };


    random_shuffle(points.begin(), points.end());
    SharedNode<float> root = createKDTree<float>(points, Options::MAX_RANGE);

    /// compare the 1NN query with brute force search
    Point<float> t0 = { 0.0, 0.0, 0.0 };
    auto kDTreeRes = queryKNN<float>(t0, root, 3);
    auto pqKDTreeRes = kDTreeRes.pq();
    auto pqBruteForceRes = bruteForceKNN(points, t0);

    auto iter1 = pqKDTreeRes.begin();
    auto iter2 = pqBruteForceRes.begin();
    EXPECT_LONGS_EQUAL(3, kDTreeRes.size());
    for (int i = 0; i < 3; ++i) {

        float dist = distance<float>((*iter1->second->pointWithIndex()).first, t0);
        size_t index = (*iter1->second->pointWithIndex()).second;

        EXPECT_DOUBLES_EQUAL(iter2->first, dist, 1e-9);
        EXPECT_LONGS_EQUAL(iter2->second, index);
        ++iter1;
        ++iter2;
    }
}

TEST(Query, queryKNN1NN) {
    PointsWithIndices<float> points = { { { 1.0, 2.0, 3.0 }, 0},
                                        { { 4.0, 5.0, 6.0 }, 1 },
                                        { { 7.0, 8.0, 9.0 }, 2 },
                                        { { 0.0, -5.0, 10.0 }, 3},
                                        { { 19.0, 0.8, -20.0 }, 4} };


    random_shuffle(points.begin(), points.end());
    SharedNode<float> root = createKDTree<float>(points, Options::MAX_RANGE);

    /// compare the 1NN query with brute force search
    Point<float> t0 = { 0.0, 0.0, 0.0 };
    auto kDTreeRes = queryKNN<float>(t0, root, 1);
    EXPECT_LONGS_EQUAL(1, kDTreeRes.size());

    auto pqKDTreeRes = kDTreeRes.pq();
    auto bruteForceRes = bruteForce1NN(points, t0);

    auto iter = pqKDTreeRes.begin();
    float dist = distance<float>((*iter->second->pointWithIndex()).first, t0);
    size_t index = (*iter->second->pointWithIndex()).second;

    EXPECT_DOUBLES_EQUAL(bruteForceRes.second, dist, 1e-9);
    EXPECT_LONGS_EQUAL(bruteForceRes.first, index);
}

TEST(IO, io) {
    PointsWithIndices<float> points = { { { 1.0, 2.0, 3.0 }, 0},
                                        { { 4.0, 5.0, 6.0 }, 1 },
                                        { { 7.0, 8.0, 9.0 }, 2 },
                                        { { 0.0, -5.0, 10.0 }, 3}};

    random_shuffle(points.begin(), points.end());
    SharedNode<float> root = createKDTree<float>(points, Options::MAX_RANGE);
    KDTreeNode<float>::saveTree(root, "test.dat");
    SharedNode<float> newRoot = KDTreeNode<float>::loadTree("test.dat");

    /// check whether roots are the same
    EXPECT_LONGS_EQUAL(root->splittingAxis(), newRoot->splittingAxis());
    EXPECT_DOUBLES_EQUAL(root->splittingValue(), newRoot->splittingValue(), 1e-9);
    EXPECT(root->left().get() != nullptr && newRoot->left().get() != nullptr);
    EXPECT(root->right().get() != nullptr && newRoot->right().get() != nullptr);
    EXPECT(root->pointWithIndex() == boost::none && newRoot->pointWithIndex() == boost::none);

    /// check original tree
    SharedNode<float> left = root->left();
    EXPECT_LONGS_EQUAL(1, left->splittingAxis());
    EXPECT_DOUBLES_EQUAL(-1.5, left->splittingValue(), 1e-9);
    EXPECT(left->left().get() != nullptr);
    EXPECT(left->right().get() != nullptr);
    EXPECT(left->pointWithIndex() == boost::none);

    SharedNode<float> right = root->right();
    EXPECT_LONGS_EQUAL(0, right->splittingAxis());
    EXPECT_DOUBLES_EQUAL(5.5, right->splittingValue(), 1e-9);
    EXPECT(right->left().get() != nullptr);
    EXPECT(right->right().get() != nullptr);
    EXPECT(right->pointWithIndex() == boost::none);

    SharedNode<float> ll = left->left();
    EXPECT_LONGS_EQUAL(0, ll->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, ll->splittingValue(), 1e-9);
    EXPECT(ll->left().get() == nullptr);
    EXPECT(ll->right().get() == nullptr);
    EXPECT(ll->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*ll->pointWithIndex()).first[0], 0.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*ll->pointWithIndex()).first[1], -5.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*ll->pointWithIndex()).first[2], 10.0, 1e-9);
    EXPECT_LONGS_EQUAL((*ll->pointWithIndex()).second, 3);

    SharedNode<float> lr = left->right();
    EXPECT_LONGS_EQUAL(0, lr->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, lr->splittingValue(), 1e-9);
    EXPECT(lr->left().get() == nullptr);
    EXPECT(lr->right().get() == nullptr);
    EXPECT(lr->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*lr->pointWithIndex()).first[0], 1.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*lr->pointWithIndex()).first[1], 2.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*lr->pointWithIndex()).first[2], 3.0, 1e-9);
    EXPECT_LONGS_EQUAL((*lr->pointWithIndex()).second, 0);

    SharedNode<float> rl = right->left();
    EXPECT_LONGS_EQUAL(0, rl->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, rl->splittingValue(), 1e-9);
    EXPECT(rl->left().get() == nullptr);
    EXPECT(rl->right().get() == nullptr);
    EXPECT(rl->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*rl->pointWithIndex()).first[0], 4.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rl->pointWithIndex()).first[1], 5.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rl->pointWithIndex()).first[2], 6.0, 1e-9);
    EXPECT_LONGS_EQUAL((*rl->pointWithIndex()).second, 1);

    SharedNode<float> rr = right->right();
    EXPECT_LONGS_EQUAL(0, rr->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, rr->splittingValue(), 1e-9);
    EXPECT(rr->left().get() == nullptr);
    EXPECT(rr->right().get() == nullptr);
    EXPECT(rr->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*rr->pointWithIndex()).first[0], 7.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rr->pointWithIndex()).first[1], 8.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rr->pointWithIndex()).first[2], 9.0, 1e-9);
    EXPECT_LONGS_EQUAL((*rr->pointWithIndex()).second, 2);

    /// check the loaded tree with the same parameters
    left = newRoot->left();
    EXPECT_LONGS_EQUAL(1, left->splittingAxis());
    EXPECT_DOUBLES_EQUAL(-1.5, left->splittingValue(), 1e-9);
    EXPECT(left->left().get() != nullptr);
    EXPECT(left->right().get() != nullptr);
    EXPECT(left->pointWithIndex() == boost::none);

    right = newRoot->right();
    EXPECT_LONGS_EQUAL(0, right->splittingAxis());
    EXPECT_DOUBLES_EQUAL(5.5, right->splittingValue(), 1e-9);
    EXPECT(right->left().get() != nullptr);
    EXPECT(right->right().get() != nullptr);
    EXPECT(right->pointWithIndex() == boost::none);

    ll = left->left();
    EXPECT_LONGS_EQUAL(0, ll->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, ll->splittingValue(), 1e-9);
    EXPECT(ll->left().get() == nullptr);
    EXPECT(ll->right().get() == nullptr);
    EXPECT(ll->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*ll->pointWithIndex()).first[0], 0.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*ll->pointWithIndex()).first[1], -5.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*ll->pointWithIndex()).first[2], 10.0, 1e-9);
    EXPECT_LONGS_EQUAL((*ll->pointWithIndex()).second, 3);

    lr = left->right();
    EXPECT_LONGS_EQUAL(0, lr->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, lr->splittingValue(), 1e-9);
    EXPECT(lr->left().get() == nullptr);
    EXPECT(lr->right().get() == nullptr);
    EXPECT(lr->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*lr->pointWithIndex()).first[0], 1.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*lr->pointWithIndex()).first[1], 2.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*lr->pointWithIndex()).first[2], 3.0, 1e-9);
    EXPECT_LONGS_EQUAL((*lr->pointWithIndex()).second, 0);

    rl = right->left();
    EXPECT_LONGS_EQUAL(0, rl->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, rl->splittingValue(), 1e-9);
    EXPECT(rl->left().get() == nullptr);
    EXPECT(rl->right().get() == nullptr);
    EXPECT(rl->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*rl->pointWithIndex()).first[0], 4.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rl->pointWithIndex()).first[1], 5.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rl->pointWithIndex()).first[2], 6.0, 1e-9);
    EXPECT_LONGS_EQUAL((*rl->pointWithIndex()).second, 1);

    rr = right->right();
    EXPECT_LONGS_EQUAL(0, rr->splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, rr->splittingValue(), 1e-9);
    EXPECT(rr->left().get() == nullptr);
    EXPECT(rr->right().get() == nullptr);
    EXPECT(rr->pointWithIndex() != boost::none);
    EXPECT_DOUBLES_EQUAL((*rr->pointWithIndex()).first[0], 7.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rr->pointWithIndex()).first[1], 8.0, 1e-9);
    EXPECT_DOUBLES_EQUAL((*rr->pointWithIndex()).first[2], 9.0, 1e-9);
    EXPECT_LONGS_EQUAL((*rr->pointWithIndex()).second, 2);

}

int main() {
   TestResult tr;
   return TestRegistry::runAllTests(tr);
}
