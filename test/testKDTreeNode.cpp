#include "../src/CppUnitLite/TestHarness.h"
#include "../src/KDTree.h"

using namespace std;
using namespace kdtree;

TEST(KDTreeNodeConstructors, defaultConstructor) {
    /// instantialized with integer
    KDTreeNode<int> nodeInt;
    EXPECT_LONGS_EQUAL(0, nodeInt.splittingAxis());
    EXPECT_LONGS_EQUAL(0, nodeInt.splittingValue());
    EXPECT(nodeInt.left().get() == nullptr);
    EXPECT(nodeInt.right().get() == nullptr);
    EXPECT(nodeInt.pointWithIndex() == boost::none);

    /// instantialized with double
    KDTreeNode<double> nodeDouble;
    EXPECT_LONGS_EQUAL(0, nodeDouble.splittingAxis());
    EXPECT_DOUBLES_EQUAL(0.0, nodeDouble.splittingValue(), 1e-9);
    EXPECT(nodeDouble.left().get() == nullptr);
    EXPECT(nodeDouble.right().get() == nullptr);
    EXPECT(nodeDouble.pointWithIndex() == boost::none);
}

TEST(KDTreeNodeConstructors, userDefinedConstructorInt) {
    PointWithIndex<int> p0 = { {10, 20, 30, 40}, 0 };
    KDTreeNode<int>::SharedKDTreeNode sharedNodeInt0 = boost::make_shared<KDTreeNode<int>>(1, 20, nullptr, nullptr, OptionalPointWithIndex<int>(p0));

    PointWithIndex<int> p1 = { {20, 30, 40, 50}, 1 };
    KDTreeNode<int>::SharedKDTreeNode sharedNodeInt1 = boost::make_shared<KDTreeNode<int>>(2, 40, nullptr, nullptr, OptionalPointWithIndex<int>(p1));

    KDTreeNode<int>::SharedKDTreeNode sharedNodeInt2 = boost::make_shared<KDTreeNode<int>>(3, 60, sharedNodeInt0, sharedNodeInt1, boost::none);

    EXPECT_LONGS_EQUAL(3, sharedNodeInt2->splittingAxis());
    EXPECT_LONGS_EQUAL(60, sharedNodeInt2->splittingValue());
    EXPECT(sharedNodeInt2->left() == sharedNodeInt0);
    EXPECT(sharedNodeInt2->right() == sharedNodeInt1);
    EXPECT(sharedNodeInt2->pointWithIndex() == boost::none);

    EXPECT_LONGS_EQUAL(1, sharedNodeInt0->splittingAxis());
    EXPECT_LONGS_EQUAL(20, sharedNodeInt0->splittingValue());
    EXPECT(sharedNodeInt0->left().get() == nullptr);
    EXPECT(sharedNodeInt0->right().get() == nullptr);
    EXPECT(sharedNodeInt0->pointWithIndex() != boost::none);
    EXPECT(*(sharedNodeInt0->pointWithIndex()) == p0);

    EXPECT_LONGS_EQUAL(2, sharedNodeInt1->splittingAxis());
    EXPECT_LONGS_EQUAL(40, sharedNodeInt1->splittingValue());
    EXPECT(sharedNodeInt1->left().get() == nullptr);
    EXPECT(sharedNodeInt1->right().get() == nullptr);
    EXPECT(sharedNodeInt1->pointWithIndex() != boost::none);
    EXPECT(*(sharedNodeInt1->pointWithIndex()) == p1);
}

TEST(KDTreeNodeConstructors, userDefinedConstructorDouble) {
    PointWithIndex<double> p0 = { {10.07, 20.38, 30.34, 40.0}, 0 };
    KDTreeNode<double>::SharedKDTreeNode sharedNodeInt0 = boost::make_shared<KDTreeNode<double>>(1, 20.04, nullptr, nullptr, OptionalPointWithIndex<double>(p0));

    KDTreeNode<double>::SharedKDTreeNode sharedNodeInt2 = boost::make_shared<KDTreeNode<double>>(3, 61, sharedNodeInt0, nullptr, boost::none);

    EXPECT_LONGS_EQUAL(3, sharedNodeInt2->splittingAxis());
    EXPECT_DOUBLES_EQUAL(61.0, sharedNodeInt2->splittingValue(), 1e-9);
    EXPECT(sharedNodeInt2->left() == sharedNodeInt0);
    EXPECT(sharedNodeInt2->right().get() == nullptr);
    EXPECT(sharedNodeInt2->pointWithIndex() == boost::none);

    EXPECT_LONGS_EQUAL(1, sharedNodeInt0->splittingAxis());
    EXPECT_DOUBLES_EQUAL(20.04, sharedNodeInt0->splittingValue(), 1e-9);
    EXPECT(sharedNodeInt0->left().get() == nullptr);
    EXPECT(sharedNodeInt0->right().get() == nullptr);
    EXPECT(sharedNodeInt0->pointWithIndex() != boost::none);
    EXPECT(*(sharedNodeInt0->pointWithIndex()) == p0);
}

int main() {
   TestResult tr;
   return TestRegistry::runAllTests(tr);
}
