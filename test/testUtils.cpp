#include "../src/CppUnitLite/TestHarness.h"
#include "../src/Utils.h"

using namespace std;
using namespace kdtree;

TEST(utils, parseCSV) {
    /// parse data from csv file
    PointsWithIndices<double> points = parseCSV<double>("../../test/data.csv");

    /// check dimension
    EXPECT_LONGS_EQUAL(3, points.size());
    EXPECT_LONGS_EQUAL(2, points[0].first.size());

    /// check parsed data points
    EXPECT_DOUBLES_EQUAL(0.32, points[0].first[0], 1e-9);
    EXPECT_DOUBLES_EQUAL(0.45, points[0].first[1], 1e-9);
    EXPECT_DOUBLES_EQUAL(1.78, points[1].first[0], 1e-9);
    EXPECT_DOUBLES_EQUAL(3.82, points[1].first[1], 1e-9);
    EXPECT_DOUBLES_EQUAL(10.0, points[2].first[0], 1e-9);
    EXPECT_DOUBLES_EQUAL(23.0, points[2].first[1], 1e-9);

    /// check parsed indices
    EXPECT_LONGS_EQUAL(0, points[0].second);
    EXPECT_LONGS_EQUAL(1, points[1].second);
    EXPECT_LONGS_EQUAL(2, points[2].second);
}

TEST(utils, transposePoints) {
    PointsWithIndices<double> points = parseCSV<double>("../../test/data.csv");
    auto transposedPoints = transposePoints(points);
    EXPECT_LONGS_EQUAL(2, transposedPoints.size());
    EXPECT_LONGS_EQUAL(3, transposedPoints[0].size());
    EXPECT_DOUBLES_EQUAL(0.32, transposedPoints[0][0], 1e-9);
    EXPECT_DOUBLES_EQUAL(0.45, transposedPoints[1][0], 1e-9);
    EXPECT_DOUBLES_EQUAL(1.78, transposedPoints[0][1], 1e-9);
    EXPECT_DOUBLES_EQUAL(3.82, transposedPoints[1][1], 1e-9);
    EXPECT_DOUBLES_EQUAL(10.0, transposedPoints[0][2], 1e-9);
    EXPECT_DOUBLES_EQUAL(23.0, transposedPoints[1][2], 1e-9);
}

TEST(utils, findMedian) {
    Point<double> pointDouble0 = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 };
    random_shuffle(pointDouble0.begin(), pointDouble0.end());
    EXPECT_DOUBLES_EQUAL(3.5, findMedian(pointDouble0), 1e-9);

    Point<double> pointDouble1 = { 1.0, 2.0, 3.0, 4.0, 5.0};
    random_shuffle(pointDouble1.begin(), pointDouble1.end());
    EXPECT_DOUBLES_EQUAL(3.0, findMedian(pointDouble1), 1e-9);

    Point<double> pointDouble2;
    EXPECT_DOUBLES_EQUAL(0.0, findMedian(pointDouble2), 1e-9);

    Point<double> pointDouble3 = { 1.0 };
    random_shuffle(pointDouble3.begin(), pointDouble3.end());
    EXPECT_DOUBLES_EQUAL(1.0, findMedian(pointDouble3), 1e-9);
}

TEST(utils, sortByAxis) {
    PointsWithIndices<double> points = { {{ 1.0, 2.0, 3.0 }, 0},
                              { { 4.0, 5.0, 6.0 }, 1 },
                              { { 7.0, 8.0, 9.0 }, 2 },
                              { { 0.0, -5.0, 10.0 }, 3}};
    random_shuffle(points.begin(), points.end());

    sortByAxis(points, 0);
    EXPECT_DOUBLES_EQUAL(0.0, points[0].first[0], 1e-9);
    EXPECT_DOUBLES_EQUAL(1.0, points[1].first[0], 1e-9);
    EXPECT_DOUBLES_EQUAL(4.0, points[2].first[0], 1e-9);
    EXPECT_DOUBLES_EQUAL(7.0, points[3].first[0], 1e-9);

    EXPECT_LONGS_EQUAL(3, points[0].second);
    EXPECT_LONGS_EQUAL(0, points[1].second);
    EXPECT_LONGS_EQUAL(1, points[2].second);
    EXPECT_LONGS_EQUAL(2, points[3].second);

    sortByAxis(points, 1);
    EXPECT_DOUBLES_EQUAL(-5.0, points[0].first[1], 1e-9);
    EXPECT_DOUBLES_EQUAL(2.0, points[1].first[1], 1e-9);
    EXPECT_DOUBLES_EQUAL(5.0, points[2].first[1], 1e-9);
    EXPECT_DOUBLES_EQUAL(8.0, points[3].first[1], 1e-9);

    EXPECT_LONGS_EQUAL(3, points[0].second);
    EXPECT_LONGS_EQUAL(0, points[1].second);
    EXPECT_LONGS_EQUAL(1, points[2].second);
    EXPECT_LONGS_EQUAL(2, points[3].second);

    sortByAxis(points, 2);
    EXPECT_DOUBLES_EQUAL(3.0, points[0].first[2], 1e-9);
    EXPECT_DOUBLES_EQUAL(6.0, points[1].first[2], 1e-9);
    EXPECT_DOUBLES_EQUAL(9.0, points[2].first[2], 1e-9);
    EXPECT_DOUBLES_EQUAL(10.0, points[3].first[2], 1e-9);

    EXPECT_LONGS_EQUAL(0, points[0].second);
    EXPECT_LONGS_EQUAL(1, points[1].second);
    EXPECT_LONGS_EQUAL(2, points[2].second);
    EXPECT_LONGS_EQUAL(3, points[3].second);
}

TEST(utils, findRange) {
    Point<double> point = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 };
    random_shuffle(point.begin(), point.end());
    auto range = findRange(point);
    EXPECT_DOUBLES_EQUAL(5.0, range, 1e-9);
}

int main() {
   TestResult tr;
   return TestRegistry::runAllTests(tr);
}
